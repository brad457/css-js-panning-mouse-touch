# Testing CSS Panning with Mouse and Touch

This repository contains various instances of CSS and/or JavaScript-based panning and/or zooming of an image inside an element or page background.

Most work in some fashion, some do not, all are slightly different iterations of testing panning.

All code contained inside this repository falls under the [Commons Clause V 1.0 License](https://gitlab.com/brad457/css-js-panning-mouse-touch/-/raw/master/LICENSE) except any parts that contains code that is licensed differently (_e.g. included software libraries, links to software libraries, or sections of used for referencing software libraries that are under a different license_).